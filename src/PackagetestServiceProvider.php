<?php

namespace Verwirrt;

use Illuminate\Support\ServiceProvider;

class PackagetestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        // 单例绑定服务
        // $this->app->singleton('packagetest', function ($app) {
        //     return new Packagetest($app['session'], $app['config']);
        // });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->publishes([
            __DIR__ . '/Lang/ServiceMakeCommand.php' => app_path('/Console/Commands/ServiceMakeCommand.php'), // 发布配置文件到 laravel 的config 下
            __DIR__ . '/Lang/stubs' => app_path('/Console/Commands/stubs'), 
            __DIR__ . '/service/Service.php' => app_path('/Http/Service/Service.php'), 
        ]);
    }
}
