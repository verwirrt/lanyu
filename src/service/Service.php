<?php

namespace App\Http\Services;

use App\Exceptions\CommonException;

class Service {
    private static $code = self::SUCCESS;
    private static $message = '成功';
    protected static $id;

    const SUCCESS = 200;
    const ERROR = 400;

    const TOKEN_ERROR = -1; // 会话超时
    const SERVICE_DISABLE = -2; // 服务器关闭
    const ADMIN_MENU_AUTH = 405; //后台访问权限
    const PARAM_ERROR = -3; // 参数错误
    const WRITE_DB_ERROR = -4; // 数据库操作失败
    const AUTH_AGAIN_ERROR = -5; // 重复认证
    const LOCK_TIME_ERROR = -6; // 超时锁

    /**
     * 返回code
     * @return int
     */
    public function code() {
        return self::$code;
    }

    /**
     * 存入用户ID
     * 在中间件中存入用户ID
     */
    public static function setID($id){
        self::$id = $id;
    }

    /**
     * 获取用户ID
     */
    public static function getId(){
        return self::$id;
    }

    /**
     * 返回message
     * @return string
     */
    public function message() {
        return self::$message;
    }

    /**
     * 返回结果
     * @param null $obj
     * @return array
     */
    public static function rs($obj = null) {
        $retData=[
            'code' => self::$code,
            'msg' => self::$message
        ];
        if ($obj){
            $retData["data"] = $obj;
        }
        return json_encode($retData, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 返回指定结果
     * @param null $obj
     * @return array
     */
    public function json($code, $message, $obj = null) {
        return json_encode([
            'code' => $code,
            'msg' => $message,
            'data' => $obj
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * 超时锁
     */
    public function lock() {
        return json_encode([
            'code' => self::LOCK_TIME_ERROR,
            'msg' => '网络异常',
            'obj' => null
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * 设置返回值
     * @param $code
     * @param $message
     */
    protected static function error( $message="请求失败" ,$code = self::ERROR) {
        self::$code = $code;
        self::$message = $message;
        return self::rs();
    }

    /**
     * 设置返回值
     * @param $code
     * @param $message
     */
    protected static function success($data = null ,$message = "",$code = self::SUCCESS) {
        self::$code = $code;
        self::$message = $message?$message:self::$message;
        return self::rs($data);
    }

    /**
     * 判断数据库返回值
     * @param $ret
     * @return bool
     */
    protected static function dbRs($ret) {
        if (!$ret) {
            throw new CommonException(self::WRITE_DB_ERROR,'数据库操作失败');
        }
    }
}
