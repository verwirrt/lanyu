<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;

class ServiceMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:lanyu {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'lanyu laravel create';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $args = $this->arguments();

        $args_name = $args['name'];

        // $this->error($args_name);
        if (strstr($args['name'], '/')) {
            $ex = explode('/', $args['name']);

            // Nihao/Sx
            // [
            //     0 => "Nihao"
            //     1 => "Sx"
            // ]
            $args_name = $ex[count($ex) - 1];
            // "Sx"

            $namespace_ext = '/' . substr($args['name'], 0, strrpos($args['name'], '/'));
            // Nihao

        }


        $namespace_ext = $namespace_ext ?? '';

        //类名
        $class_name = $args_name . 'Controller';
        $model_name = $args_name . 'Model';
        $service_name = $args_name . 'Service';

        //文件名
        $c_file_name = $class_name . '.php';
        $m_file_name = $model_name . '.php';
        $s_file_name = $service_name . '.php';

        $list = [
            ["c_file_name","\Controllers","class_name","Controllers"],
            ["s_file_name","\Service","service_name","Service"],
        ];
        foreach ($list as $v) {
            $name = $v[0];
            $namespace1 = $v[1];
            $class = $v[2];
            $type = $v[3];



            //文件地址
            $c_file = app_path() . "/Http/{$type}" . $namespace_ext . '/' . $$name;
            $namespace = "App\Http{$namespace1}". str_replace('/', '\\', $namespace_ext);


            //目录
            $c_path = dirname( $c_file);



            //获取模板,替换变量
            $template = file_get_contents(dirname(__FILE__) . "/stubs/{$type}.stub");
            // $default_method = $option ? file_get_contents(dirname(__FILE__) . '/stubs/default_method.stub') : '';
            $source = str_replace('{{namespace}}', $namespace, $template);
            $source = str_replace('{{class_name}}', $$class, $source);
            $source = str_replace('{{args_name}}', $args_name, $source);
            $source = str_replace('{{args_name}}', $namespace_ext, $source);

            //是否已存在相同文件
            if (file_exists($c_file)) {
                $this->error('文件已存在');
                exit;
            }

            //创建
            if (file_exists($c_path) === false) {
                if (mkdir($c_path, 0777, true) === false) {
                    $this->error('目录' . $c_path . '没有写入权限');
                    exit;
                }
            }

            //写入
            if (!file_put_contents($c_file, $source)) {
                $this->error('创建失败！');
                exit;
            }
        }


        $this->info('创建成功！');
    }
}
